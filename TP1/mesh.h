#ifndef MESH_H
#define MESH_H

#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

class Mesh
{
public:
    Mesh(QOpenGLShaderProgram *program);
    void createCube();
    void render();

private:
    QOpenGLBuffer m_vertexBuffer;
    QOpenGLBuffer m_indexBuffer;
    QOpenGLShaderProgram *m_program;
    int m_vertexCount;
};

#endif
