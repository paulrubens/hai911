#include "mesh.h"

Mesh::Mesh(QOpenGLShaderProgram *program) : m_program(program)
{
    m_vertexBuffer.create();
    m_indexBuffer.create();
}

void Mesh::createCube()
{
    static const float vertexData[] = { //aide de  www.math.brown.edu/tbanchof/Beyond3D/Images/chapter8/image04.jpg
        -0.1f, -0.1f, -0.1f,
        0.1f, -0.1f, -0.1f,
        0.1f,  0.1f, -0.1f,
        -0.1f,  0.1f, -0.1f,
        -0.1f, -0.1f,  0.1f,
        0.1f, -0.1f,  0.1f,
        0.1f,  0.1f,  0.1f,
        -0.1f,  0.1f,  0.1f
    };


    static const unsigned int indexData[] = {
        0, 1, 2, 2, 3, 0,
        4, 5, 6, 6, 7, 4,
        0, 1, 5, 5, 4, 0,
        2, 3, 7, 7, 6, 2,
        0, 4, 7, 7, 3, 0,
        1, 5, 6, 6, 2, 1
    };


    m_vertexCount = sizeof(indexData) / sizeof(unsigned int);

    m_vertexBuffer.bind();
    m_vertexBuffer.allocate(vertexData, sizeof(vertexData));
    m_vertexBuffer.release();

    m_indexBuffer.bind();
    m_indexBuffer.allocate(indexData, sizeof(indexData));
    m_indexBuffer.release();
}

void Mesh::render()
{

    qDebug() << "début render";

    if (!m_program) {
        qDebug() << "pas de Shader"; //pas de message d'erreur
        return;
    }

    GLenum error = glGetError();
    if(error != GL_NO_ERROR) {
        qDebug() << "OpenGL Pre-Error:"; //pas de message d'erreur
    }

    if (!m_program || !m_vertexBuffer.isCreated() || !m_indexBuffer.isCreated()) {
        qDebug() << "One of the OpenGL objects is not initialized properly."; //pas de message d'erreur
        return;
    }

    //m_program->bind();
    m_vertexBuffer.bind();
    //m_program->enableAttributeArray(0);
    //m_program->setAttributeBuffer(0, GL_FLOAT, 0, 3);

    m_indexBuffer.bind();

    //glDrawElements(GL_TRIANGLES, m_vertexCount, GL_UNSIGNED_INT, nullptr); //origine de l'erreur

    error = glGetError();
    if(error != GL_NO_ERROR) {
        qDebug() << "OpenGL Post-Error:"; //pas de message d'erreur
    }

    qDebug() << "fin render";
}


