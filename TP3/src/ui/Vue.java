package ui;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JPanel;

import geo.Geste;
import geo.PointVisible;
import ui.config.Parameters;

public class Vue extends JPanel {
	Color bgColor;
	Color fgColor; 
	int width, height;
	ArrayList<Geste> gestes;
		
	public Vue(int width, int height) {
		super();
		this.bgColor = Couleur.bg; 
		this.fgColor = Couleur.fg; 
		this.width = width;
		this.height = height;	
		this.setBackground(Couleur.bg);
		this.setPreferredSize(new Dimension(width, width));
		Tracker t = new Tracker(this);
		this.addMouseListener(t);
		this.addMouseMotionListener(t);
		gestes = new ArrayList<Geste>();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaintMode(); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,	RenderingHints.VALUE_ANTIALIAS_ON);	
		g2d.setColor(fgColor);
		
		for (Geste go: gestes) {
			go.draw(g2d);
		}
	}

	public void add(Geste geste) {
		gestes.add(geste);
	}

	public void loadData(String fileName) {
		gestes = new ArrayList<Geste>();
		File wd = new File(fileName);
		if (wd.isDirectory()) {
			for (File f:wd.listFiles()) {
				if (!(f.isDirectory())){
					add(new Geste(f.getAbsolutePath()));
					repaint();
				}
			}
		}else {
			add(new Geste(fileName));
			repaint();
		}
	}

	public void exportData(String fileName) {
		boolean createFile = new File(fileName).isDirectory();
		int i = 0;
		if (createFile) {
			for (Geste g:gestes) {
					g.exportWhenPossible(fileName+File.separator+Parameters.baseGestureFileName+"-"+i+".csv");
					i++;
			}
		} else if (gestes.size() == 1){
			gestes.get(0).exportWhenPossible(fileName);
		} 
	}
	
	public void clear() {
		gestes.clear();
		repaint();
	}
	
	public void resample() {
		MainWindow frame = new MainWindow("Resampling");
		for (int i = 0; i < gestes.size(); i++) {
			frame.addGesture(gestes.get(i).oResample());
		}
		frame.setVisible(true);
	}

	/*
	ROTATE-T O-Z ERO(points)
	1 c ← CENTROID(points) // computes (x¯, y¯)
	2 θ ← ATAN (c y – points0y, c x – points0x) // for -π ≤ θ ≤ π
	3 newPoints ← ROTATE-B Y(points, -θ)
	4 return newPoints
	ROTATE-B Y(points, θ)
	1 c ← C ENTROID(points)
	2 foreach point p in points do
	3 qx ← (px – c x) COS θ – (p y – c y) SIN θ + c x
	4 qy ← (px – c x) SIN θ + (p y – c y) COS θ + c y
	5 APPEND(newPoints, q)
	6 return newPoints
	 */
	public void rotateToZero() {
		for (Geste geste : gestes) {
			ArrayList<PointVisible> points = geste.getPoints();
			if (points.size() <= 1) continue; // Ignorer les gestes trop petits

			// Utiliser la méthode de la classe Geste pour obtenir l'orientation
			double theta = geste.calculateGestureOrientation();

			// Étape 3: Tourner tous les points par -θ
			ArrayList<PointVisible> newPoints = new ArrayList<>();
			Point2D.Double centroid = geste.updateGravity();
			for (PointVisible point : points) {
				double qx = (point.x - centroid.x) * Math.cos(-theta) - (point.y - centroid.y) * Math.sin(-theta) + centroid.x;
				double qy = (point.x - centroid.x) * Math.sin(-theta) + (point.y - centroid.y) * Math.cos(-theta) + centroid.y;
				newPoints.add(new PointVisible(qx, qy));
			}

			// Mettre à jour les points du geste
			geste.setPoints(newPoints);
		}
	}

	public void rescale() {
		// TODO Auto-generated method stub		
	}

	public void recenter() {
		// TODO Auto-generated method stub		
	}

}
