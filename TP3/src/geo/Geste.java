package geo;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import ui.Style;
import ui.config.Parameters;
import ui.io.ReadWritePoint;

public class Geste {
    private ArrayList<PointVisible> points;
    private Style style = new Style();

    public Geste() {
        points = new ArrayList<PointVisible>();
    }

    public Geste(String fileName) {
        this();
        ReadWritePoint rwp = new ReadWritePoint(new File(fileName));
        points = rwp.read();
    }

    public Geste(Style style2) {
        this();
        style = style2;
    }

    public ArrayList<PointVisible> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<PointVisible> newPoints) {
        this.points = newPoints;
    }


    public void add(Point p) {
        add(new PointVisible(p.x, p.y));
    }

    public void add(PointVisible p) {
        points.add(p);
    }

    public void drawPoints(Graphics2D g) {
        for (PointVisible p : points) {
            p.dessine(g, style);
        }
    }

    public void drawFeatures(Graphics2D g) {
        Rectangle r = computeBoundingBox();
        String features = points.size() + " points,  length ~> " + Math.round(computeLength());
        g.translate(-r.x, -r.y);
        g.scale(2, 2);
        g.drawString(features, r.x, r.y - 10);
        g.scale(.5, .5);
        g.translate(r.x, r.y);
    }

    public void draw(Graphics2D g) {
        if (style.drawLine()) {
            drawLines(g);
        }
        if (style.drawPoints()) {
            drawPoints(g);
        }
        drawFeatures(g);
    }

    private void drawLines(Graphics2D g) {
        PointVisible p1, p2;
        for (int i = 0; i < points.size() - 1; i++) {
            p1 = points.get(i);
            p2 = points.get(i + 1);
            g.drawLine(p1.x, p1.y, p2.x, p2.y);
        }
    }

    public Rectangle computeBoundingBox() {
        int minx, miny, maxx, maxy;
        minx = points.get(0).x;
        maxx = points.get(0).x;
        miny = points.get(0).y;
        maxy = points.get(0).y;
        for (PointVisible p : points) {
            if (p.x < minx) minx = p.x;
            if (p.y < miny) miny = p.y;
            if (p.x > maxx) maxx = p.x;
            if (p.y > maxy) maxy = p.y;
        }
        return new Rectangle(minx, miny, maxx - minx, maxy - miny);
    }

    //site mountaz: https://www.lirmm.fr/~mountaz/Ens/M2/td-geste-unistroke/index.htm

    public Geste rotate(double angle) {
        Geste rotatedGeste = new Geste();
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        double x, y;
        for (PointVisible p : points) {
            x = (double) p.x;
            y = (double) p.y;
            x = cos * x - sin * y;
            y = sin * x + cos * y;
            rotatedGeste.add(new PointVisible((int) x, (int) y));
        }
        return rotatedGeste;
    }

    public void exportWhenPossible(String filePath) {
        Path p = Paths.get(filePath);
        if (Files.exists(p)) {
            JOptionPane.showMessageDialog(null, "Error:" + p.getFileName() + ", file exists, no overwrite, choose an empty directory");
        } else {
            export(filePath);
        }
    }

    private void export(String path) {
        ReadWritePoint rw = new ReadWritePoint(new File(path));
        for (PointVisible p : points) {
            rw.add((int) p.x + ";" + (int) p.y + ";" + p.toString());
        }
        rw.write();
    }

    //Calculer la longueur du geste (de la polyligne du tracé)
    private float computeLength() {
        //return this.points.size();
        float length = 0;
        for(int i = 0; i < (this.points.size() - 1); i++){
            length += (float) this.points.get(i).distance(this.points.get(i+1));
        }
        return length;
    }

    /*
    RESAMPLE(points, n)
    1 I ← PATH-L ENGTH(points) / (n – 1)
    2 D ← 0
    3 newPoints ← points0
    4 foreach point p i for i ≥ 1 in points do
    5 d ← DISTANCE(p i-1, pi)
    6 if (D + d) ≥ I then
    7 qx ← p i-1x + ((I – D) / d) × (p ix – pi-1x)
    8 qy ← p i-1y + ((I – D) / d) × (p iy – pi-1y)
    9 APPEND(newPoints, q)
    10 I NSERT(points, i, q) // q will be the next p i
    11 D ← 0
    12 else D ← D + d
    13 return newPoints
    PATH-L ENGTH(A)
    1 d ← 0
    2 for i from 1 to |A| step 1 do
    3 d ← d + DISTANCE(A i-1, A i)
    4 return d
     */

    //Creer un geste ré-échantillonné au nombre de points utiles pour l'algo OneDollarRecognizer
    public Geste oResample() {
        if(this.points.size() < 1){
            return this;
        }

        int k = Parameters.OneDollarSampleSize;
        float I = computeLength() / (float)(k - 1);
        float D = 0;
        Geste newGeste = new Geste();
        ArrayList<PointVisible> newPoints = new ArrayList<>();
        newPoints.add(this.points.get(0).copy());

        for (int i = 1; i < this.points.size(); i++) {
            float d = (float) this.points.get(i).distance(this.points.get(i - 1));

            if (D + d >= I) {
                double qx = this.points.get(i - 1).x + ((I - D) / d) * (this.points.get(i).x - this.points.get(i - 1).x);
                double qy = this.points.get(i - 1).y + ((I - D) / d) * (this.points.get(i).y - this.points.get(i - 1).y);

                PointVisible q = new PointVisible(qx, qy);
                newPoints.add(q);
                this.points.add(i, q);
                D = 0;
            } else {
                D += d;
            }
        }

        newGeste.points = newPoints;
        return newGeste;
    }

    //Calculer le centre de gravité et le sauvegarder
    public Point2D.Double updateGravity() {
        Point2D.Double centroide = new Point2D.Double();
        for(int i = 0; i < this.points.size(); i++){
            centroide.x += this.points.get(i).x;
            centroide.y += this.points.get(i).y;
        }
        centroide.x /= this.points.size();
        centroide.y /= this.points.size();
        return centroide;
    }

    public double calculateGestureOrientation() {
        if (this.points.size() <= 1) {
            return 0.0;
        }

        Point2D.Double centroide = updateGravity();

        double x1 = this.points.get(0).x;
        double y1 = this.points.get(0).y;

        double angle = Math.atan2(centroide.y - y1, centroide.x - x1);

        return angle;
    }


    //Creer un geste avec une orientation à alpha
    public Geste oRotateTo(double alpha) {
        return this; // à modifier...
    }

    //Creer un geste redimensionné
    public Geste oRescale() {
        return this; // à modifier...
    }

    //Creer un geste recentré sur le centre de gravité
    public Geste oRecenter() {
        return this; // à modifier...
    }
}
