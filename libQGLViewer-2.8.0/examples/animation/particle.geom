#version 430

layout ( points ) in;

// Shader de passage : passe de point vers point
//layout ( points ) out;
// Shader utile : passe de point vers triangles
layout ( triangle_strip, max_vertices = 4 ) out;

// Les matrices peuvent être réutilisées
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

// Variables d'entrées : des tableaux
in vec3 initialVertPos[];
in float vertAge[];

// Variables de sorties : des éléments uniques
out vec2 UV;
out float fragAge;

//Geometry Shader entry point
void main(void) {
	// Taille d'une particule
	float scale = 0.005;

	vec3 up = vec3(0, scale, 0);
    vec3 right = vec3(scale, 0, 0);

    vec4 centerPos = gl_in[0].gl_Position;
	
	//gl_Position = gl_in[0].gl_Position;
	//EmitVertex();

	//sommets du quad
    gl_Position = proj_matrix * mv_matrix * (centerPos + vec4(-right - up, 0.0));
    UV = vec2(0.0, 0.0);
    EmitVertex();

    gl_Position = proj_matrix * mv_matrix * (centerPos + vec4(right - up, 0.0));
    UV = vec2(1.0, 0.0);
    EmitVertex();

    gl_Position = proj_matrix * mv_matrix * (centerPos + vec4(-right + up, 0.0));
    UV = vec2(0.0, 1.0);
    EmitVertex();

    gl_Position = proj_matrix * mv_matrix * (centerPos + vec4(right + up, 0.0));
    UV = vec2(1.0, 1.0);
    EmitVertex();

	EndPrimitive();

	fragAge = vertAge[0];
}
