#version 430

out vec4 fragColor;

// Input du geometry shader
in vec2 UV;
in float fragAge;

uniform vec4 color = vec4(1.0, 0.0, 0.0, 1.0);

uniform sampler2D textureSmoke;

void main(void)
{
    /*fragColor = color;
	
	// Utiliser les coordonnées UV pour débugger : un cadre noir autour des particules
	if (UV.x < 0.1 || UV.x > 0.9 || UV.y < 0.1 || UV.y > 0.9)
		fragColor = vec4(0.0, 0.0, 0.0, 1.0);*/

	vec4 texColor = texture(textureSmoke, UV);
    if (texColor.a < 0.1) // transparence
        discard;
    
    fragColor = texColor;

    //tentative changement de la couleur en fonction de l'âge
    //devient plus rouge en fonction de l'âge
    //vec4 ageColor = vec4(fragAge, 0.0, 0.0, 1.0);
    //fragColor = texColor * ageColor;

    //fragColor = ageColor; // entièrement rouge pour tester
}
