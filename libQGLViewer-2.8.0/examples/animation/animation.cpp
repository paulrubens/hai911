/****************************************************************************

 Copyright (C) 2002-2014 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.7.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License 
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain 
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

#include "animation.h"
#include <math.h>
#include <stdlib.h> // RAND_MAX

#include <QFile>
#include <QKeyEvent> //touche r pour réinitialiser l'emplacement de la caméra
#include <QTextStream>
#include <QOpenGLContext>

using namespace qglviewer;
using namespace std;

///////////////////////   V i e w e r  ///////////////////////
void Viewer::init() {
  restoreStateFromFile();
  nbPart_ = 2000;
  particle_ = new Particle[nbPart_];
  glPointSize(3.0);

  timer.start();

  std::string path = "./";
  std::string vShaderPath = path + "particle.vert";
  std::string gShaderPath = path + "particle.geom";
  std::string fShaderPath = path + "particle.frag";


  glContext = QOpenGLContext::currentContext();
  glFunctions = glContext->extraFunctions();
  glEnable              ( GL_DEBUG_OUTPUT );
  glFunctions->glDebugMessageCallback(&Viewer::MessageCallback, 0 );

  glEnable(GL_LIGHT0);
  glEnable(GL_LIGHT1);
  glEnable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_TEXTURE_3D);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

  // Create programs and link shaders
  this->programID = glFunctions->glCreateProgram();
  std::string content = readShaderSource(vShaderPath);
  if (!content.empty()) {
      this->vShader = glFunctions->glCreateShader(GL_VERTEX_SHADER);
      const char* src = content.c_str();
      glFunctions->glShaderSource(this->vShader, 1, &src, NULL);
      glFunctions->glCompileShader(this->vShader);
      glFunctions->glAttachShader(this->programID, this->vShader);
      printShaderErrors(this->vShader);
  }
  content = readShaderSource(fShaderPath);
  if (!content.empty()) {
      this->fShader = glFunctions->glCreateShader(GL_FRAGMENT_SHADER);
      const char* src = content.c_str();
      glFunctions->glShaderSource(this->fShader, 1, &src, NULL);
      glFunctions->glCompileShader(this->fShader);
      glFunctions->glAttachShader(this->programID, this->fShader);
      printShaderErrors(this->fShader);
  }
  /*
   * Code à décommenter pour utiliser des geometry shader*/
  content = readShaderSource(gShaderPath);
  if (!content.empty()) {
      this->gShader = glFunctions->glCreateShader(GL_GEOMETRY_SHADER);
      const char* src = content.c_str();
      glFunctions->glShaderSource(this->gShader, 1, &src, NULL);
      glFunctions->glCompileShader(this->gShader);
      glFunctions->glAttachShader(this->programID, this->gShader);
      printShaderErrors(this->gShader);
  }

  //similaire aux tps M1
  glGenTextures(1, &SmokeID);
  glBindTexture(GL_TEXTURE_2D, SmokeID);

  QImage textureImage("/home/gharnef/hai911/libQGLViewer-2.8.0/smoke.png");
  QImage formattedImage = textureImage.convertToFormat(QImage::Format_RGBA8888);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, formattedImage.width(), formattedImage.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, formattedImage.bits());
  glFunctions->glGenerateMipmap(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, 0);

  glFunctions->glUseProgram(programID);
  glFunctions->glUniform1i(glFunctions->glGetUniformLocation(programID, "textureSmoke"), 0);
  glFunctions->glUseProgram(0);

  glFunctions->glLinkProgram(this->programID);
  glFunctions->glUseProgram(programID);
  printProgramErrors(programID);
  checkOpenGLError();

  glFunctions->glGenBuffers(1, &particlePosBuffer);
  glFunctions->glBindBuffer(GL_ARRAY_BUFFER, particlePosBuffer);
  checkOpenGLError();

  startAnimation();
}

void Viewer::resetCamera() {
  camera()->setPosition(qglviewer::Vec(0, 0, 0)); // Position initiale
  camera()->setUpVector(qglviewer::Vec(0, 1, 0)); // Vector up
  camera()->lookAt(qglviewer::Vec(0, 0, 0)); // Point de vue
  update();
}

void Viewer::keyPressEvent(QKeyEvent *event) {
  // Vérifier si la touche 'R' est pressée
  if (event->key() == Qt::Key_R) {
      resetCamera();
  }

  // Appeler la méthode parente pour gérer d'autres événements clés
  QGLViewer::keyPressEvent(event);
}

void Viewer::draw() {
    particlesPos.clear();
    for (int i = 0; i < nbPart_; i++) {
        //age de la particule
        float ageRatio = static_cast<float>(particle_[i].age_) / particle_[i].ageMax_;
        //std::cout << ageRatio << std::endl; //ratio ok
        glFunctions->glUniform1f(glFunctions->glGetUniformLocation(programID, "age"), ageRatio);

        particlesPos.push_back(particle_[i].pos_.x);
        particlesPos.push_back(particle_[i].pos_.y);
        particlesPos.push_back(particle_[i].pos_.z);
    }
    // Coordonnées des particules vers le GPU
    glFunctions->glBindBuffer(GL_ARRAY_BUFFER, particlePosBuffer);
    glFunctions->glBufferData(GL_ARRAY_BUFFER, particlesPos.size() * sizeof(float), &particlesPos[0], GL_STATIC_DRAW);
    glFunctions->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glFunctions->glEnableVertexAttribArray(0);

    //smoke
    glFunctions->glActiveTexture(GL_TEXTURE0);
    glFunctions->glBindTexture(GL_TEXTURE_2D, SmokeID);


    // Récuperation des matrices de projection / vue-modèle
    float pMatrix[16];
    float mvMatrix[16];
    camera()->getProjectionMatrix(pMatrix);
    camera()->getModelViewMatrix(mvMatrix);
    glFunctions->glUniformMatrix4fv(glFunctions->glGetUniformLocation(programID, "proj_matrix"),
                                       1, GL_FALSE, pMatrix);
    glFunctions->glUniformMatrix4fv(glFunctions->glGetUniformLocation(programID, "mv_matrix"),
                                       1, GL_FALSE, mvMatrix);


    // Passage de variables uniformes, comme d'hab
    float color[4] = {1.f, 0.f, 0.f, 1.f};
    glFunctions->glUniform4fv(glFunctions->glGetUniformLocation(programID, "color"),
                             1, color);

    glFunctions->glDrawArrays(GL_POINTS, 0, particlesPos.size());
    checkOpenGLError();
}

void /*GLAPIENTRY */Viewer::MessageCallback( GLenum source, GLenum type,
                                            GLuint id, GLenum severity,
                                            GLsizei length, const GLchar* message,
                                            const void* userParam )
{
    if (severity == GL_DEBUG_SEVERITY_HIGH || severity == GL_DEBUG_SEVERITY_MEDIUM || severity == GL_DEBUG_SEVERITY_LOW) {
        std::string s_severity = (severity == GL_DEBUG_SEVERITY_HIGH ? "High" : severity == GL_DEBUG_SEVERITY_MEDIUM ? "Medium" : "Low");
        std::cout << "Error " << id << " [severity=" << s_severity << "]: " << message << std::endl;
    }
}
bool Viewer::checkOpenGLError()
{
    bool error = false;
    int glErr = glGetError();
    while(glErr != GL_NO_ERROR)
    {
        std::cout << "[OpenGL] Error: " << glErr << std::endl;
        error = true;
        glErr = glGetError();
    }
    return !error;
}

bool Viewer::printShaderErrors(GLuint shader)
{
    int state = 0;
    glFunctions->glGetShaderiv(shader, GL_COMPILE_STATUS, &state);
    if (state == 1)
        return true;
    int len = 0;
    int chWritten = 0;
    char* log;
    glFunctions->glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    if (len > 0)
    {
        log = (char*)malloc(len);
        glFunctions->glGetShaderInfoLog(shader, len, &chWritten, log);
        std::cout << "[OpenGL] Shader error: " << log << std::endl;
        free(log);
    }
    return false;
}
bool Viewer::printProgramErrors(int program)
{
    int state = 0;
    glFunctions->glGetProgramiv(program, GL_LINK_STATUS, &state);
    if (state == 1)
        return true;
    int len = 0;
    int chWritten = 0;
    char* log;
    glFunctions->glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
    if (len > 0)
    {
        log = (char*)malloc(len);
        glFunctions->glGetProgramInfoLog(program, len, &chWritten, log);
        std::cout << "[OpenGL] Program error: " << log << std::endl;
        free(log);
    }
    return false;
}

std::string Viewer::readShaderSource(std::string filename)
{
    std::string content = "";
    QString qFilename = QString::fromStdString(filename);
    if (!QFile::exists(qFilename))
        qFilename = ":" + qFilename;
    if (!QFile::exists(qFilename)) {
        std::cerr << "The shader " << filename << " doesn't exist!" << std::endl;
        return "";
    }
    QFile file(qFilename);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    std::string line;
    QTextStream in(&file);
    while (!in.atEnd()) {
        line = in.readLine().toStdString();
        content += line + " \n";
    }
    file.close();
    return content;
}

void Viewer::animate() {
    float deltaT = timer.elapsed() / 1000.0f;
    timer.restart();

    for (int i = 0; i < nbPart_; i++){
        particle_[i].animate(deltaT);
    }
}

QString Viewer::helpString() const {
  QString text("<h2>A n i m a t i o n</h2>");
  text += "Use the <i>animate()</i> function to implement the animation part "
          "of your ";
  text += "application. Once the animation is started, <i>animate()</i> and "
          "<i>draw()</i> ";
  text += "are called in an infinite loop, at a frequency that can be "
          "fixed.<br><br>";
  text += "Press <b>Return</b> to start/stop the animation.";
  return text;
}

///////////////////////   P a r t i c l e   ///////////////////////////////

Particle::Particle() { init(); }

void Particle::animate(float deltaT) {
  const Vec gravity(0.0, 0.0, -9.81);
  speed_ += gravity * deltaT;
  pos_ += speed_ * deltaT;

  // rebond si pos < 0
  if (pos_.z < 0.0) {
        speed_.z = -0.8 * speed_.z;
        pos_.z = 0.0;
  }

  if (++age_ >= ageMax_) {
        init();
  }
}

void Particle::draw() {
  glColor3f(age_ / (float)ageMax_, age_ / (float)ageMax_, 1.0);
  glVertex3fv(pos_);
}

void Particle::init() {
  pos_ = Vec(0.0, 0.0, 0.0);
  float angle = 2.0 * M_PI * rand() / RAND_MAX;
  float norm = 0.04 * rand() / RAND_MAX;
  speed_ = Vec(norm * cos(angle), norm * sin(angle),
               rand() / static_cast<float>(RAND_MAX));
  age_ = 0;
  ageMax_ = 50 + static_cast<int>(100.0 * rand() / RAND_MAX);
}
